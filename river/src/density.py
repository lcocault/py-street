#!/usr/bin/env python3
""" Compute the density of first digits for a given period
Usage:
    See "help"
"""

#    Copyright 2020 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


def compute_density(max):
    print("Computing for {0}".format(max))
    counts = {}
    for i in range(max):
        firstDigit = "{0}".format(i)[0]
        try:
            counts[firstDigit] = counts[firstDigit] + 1
        except KeyError:
            counts[firstDigit] = 1
    for i in counts:
        print("{0} => {1}".format(i, 100*counts[i]/max))


if __name__ == '__main__':
    compute_density(20)
    compute_density(50)
    compute_density(100)
    compute_density(150)
    compute_density(200)
