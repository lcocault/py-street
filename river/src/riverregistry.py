#!/usr/bin/env python3
""" Extract data from the river registry
Usage:
    See "help"
"""

#    Copyright 2020 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from wikiarchive import WikiArchive
from wikidownload import download_wikidata_entity


def select_all(url):
    # Rivers have a type Q4022 in the archive
    archive = WikiArchive(url)
    entities = archive.search("type", "Q4022", 10000)
    # But an additional selection from the original source is necessary to get
    # the length of each river
    rivers = []
    for entity in entities:
        rivers.append(download_wikidata_entity(entity.identifier()))
    return rivers


if __name__ == '__main__':
    # Select all the knwon rivers from Wikipedia
    rivers = select_all("localhost:9200")
    # Init the numerical facts
    minLength = 40000.0
    maxLength = 0.0
    sum = 0.0
    count = len(rivers)
    counts = {}
    countsMiles = {}
    # Iterate on rivers to compute actual river length facts
    for river in rivers:
        try:
            length = river.length_in_km()
            minLength = min(minLength, length)
            maxLength = max(maxLength, length)
            sum = sum + length
            firstDigit = "{0}".format(length)[0]
            firstDigitMiles = "{0}".format(length/1.609)[0]
            try:
                counts[firstDigit] = counts[firstDigit] + 1
                countsMiles[firstDigitMiles] = countsMiles[firstDigitMiles] + 1
            except KeyError:
                counts[firstDigit] = 1
                countsMiles[firstDigitMiles] = 1
        except Exception:
            pass
    # Display the facts
    print("Min: {0}".format(minLength))
    print("Max: {0}".format(maxLength))
    print("Avg: {0}".format(sum/count))
    print(counts)
    print(countsMiles)
