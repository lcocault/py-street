#!/usr/bin/env python3
""" Test the River Registry """

#    Copyright 2020 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from unittest import TestCase
from riverregistry import select_all


class TestRegistry(TestCase):

    def test_all_rivers(self):
        rivers = select_all("localhost:9200")
        self.assertEqual(len(rivers), 5316)
