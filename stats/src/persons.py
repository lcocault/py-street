#!/usr/bin/env python3
""" Statistics based on person names
Usage:
    See "help"
"""

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from pandas import DataFrame
import plotly.express as px
import sys

from streetregistry import parse_database
from wikiarchive import WikiArchive

ARMES = {"colonel", "corsaire", "général", "guerre", "maréchal", "militaire",
         "officier", "résistan"}
ARTS = ("acteur", "actrice", "archi", "artist", "chant", "ciné", "coméd",
        "composit", "danse", "dessin", "humor", "interprète", "jazz", "musi",
        "peintre", "photo", "pian", "sculpt", "troub", "violon")
LETTRES = ("auteur", "dramaturge", "encyclo", "écrivain", "histor", "journal",
           "lettres", "poète", "philosophe", "scénar", "roman")
POLITIQUE = ("baron", "chanceli", "comte", "député", "diplomate", "duc",
             "état", "juris", "ministre", "politi", "président", "prince",
             "sénat")
RELIGION = ("église", "évêque", "mission", "pape", "pasteur", "prélat",
            "prêtre", "prophète", "saint")
SCIENCES = ("aéro", "archéo", "astro", "aviat", "biolog", "botanis", "chimi",
            "chirurg", "cosmo", "économ", "égypt", "explorat", "immun",
            "ingénieur", "invent", "mathémati", "médecin", "naturalis",
            "pharma", "psych", "physi", "spatio", "zoo")
SPORTS = ("alpin", "boxe", "coureu", "cycl", "escri", "football", "joueu",
          "nage", "natatio", "navigat", "pilote", "rugby", "ski", "sport")


def parse_category(description):
    if any(business in description for business in ARMES):
        category = "ARMES"
    elif any(business in description for business in ARTS):
        category = "ARTS"
    elif any(business in description for business in LETTRES):
        category = "LETTRES"
    elif any(business in description for business in POLITIQUE):
        category = "POLITIQUE"
    elif any(business in description for business in RELIGION):
        category = "RELIGION"
    elif any(business in description for business in SCIENCES):
        category = "SCIENCES"
    elif any(business in description for business in SPORTS):
        category = "SPORTS"
    else:
        category = "INCONNU"
    return category


if __name__ == '__main__':
    # Streets
    streets = parse_database("../../street/data/base-adresse-locale.json")
    # Persons and categories
    archive = WikiArchive("localhost:9200")
    categories = {}
    genres = {}
    lats = []
    lons = []
    cats = []
    gens = []
    # Match street names being countries
    for street in streets:
        persons = archive.search("label", street.name(), 1)
        try:
            if len(persons) > 0:
                # Compute data related to the person
                if persons[0].genre() == 'Q6581097':
                    genre = 'H'
                else:
                    genre = 'F'
                category = parse_category(persons[0].description().lower())

                # Compute stats
                try:
                    categories[category] = categories[category] + 1
                except KeyError:
                    categories[category] = 1
                try:
                    genres[genre] = genres[genre] + 1
                except KeyError:
                    genres[genre] = 1

                # Record data
                if category == "INCONNU":
                    print(street.fullname())
                    sys.stdout.flush()
                else:
                    lon = street.position()[0] - 1.265
                    lat = street.position()[1] - 43.450
                    cats.append(category)
                    gens.append(genre)
                    lats.append(int(lat * 1000))
                    lons.append(int(lon * 1000))

        except AttributeError:
            pass

    # Draw categories
    square = {'x': lons, 'y': lats, 'color': cats}
    df = DataFrame(square, columns=['x', 'y', 'color'])
    fig = px.scatter(df, x='x', y='y', color='color', size_max=60)
    fig.write_image("persons.png", width=900, height=1050)
    # Draw genres
    square = {'x': lons, 'y': lats, 'color': gens}
    df = DataFrame(square, columns=['x', 'y', 'color'])
    fig = px.scatter(df, x='x', y='y', color='color', size_max=60)
    fig.write_image("genres.png", width=900, height=1050)
    # Display synthesis
    for category in categories:
        print("Catégorie {} : {}".format(category, categories[category]))
    for genre in genres:
        print("Genre {} : {}".format(genre, genres[genre]))
