#!/usr/bin/env python3
""" Statistics based on street types
Usage:
    See "help"
"""

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import plotly.graph_objects as go

from streetregistry import parse_database

if __name__ == '__main__':
    # Streets
    streets = parse_database("../../street/data/base-adresse-locale.json")
    # Match street names being countries
    categories = {}
    for street in streets:
        category = street.category()
        try:
            categories[category] = categories[category] + 1
        except KeyError:
            categories[category] = 1
    # Build synthesis
    labels = []
    values = []
    othersvalue = 0
    for category in categories:
        count = categories[category]
        print("{} => {}".format(category, count))
        if count > 75:
            labels.append(category)
            values.append(count)
        else:
            othersvalue += count
    labels.append("autres")
    values.append(othersvalue)
    # Generate the pie chart
    fig = go.Figure(data=[go.Pie(labels=labels, values=values)])
    fig.write_image("types.png")
