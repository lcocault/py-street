Le fichier base-adresse-locale.json est issu du portail des données libres de
Toulouse Métropole. Ces données originales ont été téléchargées sur
https://data.toulouse-metropole.fr/explore/dataset/base-adresse-locale/export/,
mise à jour du 24 août 2019.
