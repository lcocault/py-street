#!/usr/bin/env python3
""" Model of data from the streets registry
Usage:
    See "help"
"""

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


class Street:
    CATEGORIES = ("allée",
                  "avenue",
                  "boulevard",
                  "chemin",
                  "clos",
                  "cours",
                  "espace",
                  "esplanade",
                  "impasse",
                  "lotissement",
                  "passage",
                  "place",
                  "pont",
                  "porte",
                  "promenade",
                  "rond-point",
                  "route",
                  "rue",
                  "square")
    PARTICLES = ("d", "de", "des", "du", "la", "l")

    def __init__(self, fullname):
        self._fullname = fullname
        self._addresses = []
        self.parse(fullname)

    def add(self, address):
        self._addresses.append(address)

    def category(self):
        return self._category

    def fullname(self):
        return self._fullname

    def addresses(self):
        return self._addresses

    def name(self):
        return self._name

    def parse(self, fullname):
        # Extract the category
        fullname = fullname.lower()
        fullname = fullname.replace("allées", "allée")
        fullname = fullname.replace("cheminement", "chemin")
        fullname = fullname.replace("cité", "lotissement")
        fullname = fullname.replace("giratoire", "rond-point")
        fullname = fullname.replace("le clos", "clos")
        fullname = fullname.replace("petite allée", "allée")
        fullname = fullname.replace("résidence", "lotissement")
        category = fullname.split()[0]
        if category in Street.CATEGORIES:
            self._category = category
        else:
            self._category = "unknown"

        # Remove the particles/articles
        fullname = fullname[len(category)+1:]
        self._name = self.remove_particles(fullname)

    def position(self):
        loc = self.addresses()[0]
        return [loc.longitude(), loc.latitude()]

    def remove_particles(self, value):
        words = value.replace("'", " ").split()
        i = 0
        offset = 0
        while i < len(words) and words[i] in Street.PARTICLES:
            offset = offset + len(words[i]) + 1
            i += 1
        return value[offset:]


class Address:

    def __init__(self, street, number, longitude, latitude):
        self._street = street
        self._number = number
        self._longitude = longitude
        self._latitude = latitude
        street.add(self)

    def street(self):
        return self._street

    def latitude(self):
        return self._latitude

    def longitude(self):
        return self._longitude
