#!/usr/bin/env python3
""" Extract data from the street registry
Usage:
    See "help"
"""

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import json

from street import Address
from street import Street


def parse_streets(json_content):
    """Parse the streets from the given JSON content

    Args:
        json_content: JSON content representing the streets

    Returns:
        The parsed streets
    """
    streets = {}
    data = json.loads(json_content)
    for json_street in data:
        # Extract fields from the parsed element
        name = json_street["fields"]["voie_nom"]
        number = json_street["fields"]["numero"]
        latitude = json_street["fields"]["lat"]
        longitude = json_street["fields"]["long"]
        # Get or create the street
        try:
            street = streets[name]
        except KeyError:
            street = Street(name)
            streets[name] = street
        # Create the address
        Address(street, number, longitude, latitude)
    return streets.values()


def parse_database(path):
    """Extract the content of the database

    Args:
        path: Path of the database to parse

    Returns:
        The list of streets in the database
    """
    with open(path) as json_file:
        return parse_streets(json_file.read())


if __name__ == '__main__':
    streets = parse_database("./data/base-adresse-locale.json")
    for street in streets:
        print(street.name())
