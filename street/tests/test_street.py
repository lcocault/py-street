#!/usr/bin/env python3
""" Test the Street classes """

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from unittest import TestCase
from street import Address
from street import Street


class TestStreet(TestCase):

    def test_fullname(self):
        street = Street('Impasse Saint-Perier')
        self.assertEqual(street.fullname(), "Impasse Saint-Perier")

    def test_category_allee(self):
        street = Street('Allée de Bellefontaine')
        self.assertEqual(street.category(), "allée")

    def test_category_avenue(self):
        street = Street('Avenue Etienne Billières')
        self.assertEqual(street.category(), "avenue")

    def test_category_boulevard(self):
        street = Street('Boulevard Déodat de Séverac')
        self.assertEqual(street.category(), "boulevard")

    def test_category_chemin(self):
        street = Street('Chemin Dorade')
        self.assertEqual(street.category(), "chemin")

    def test_category_clos(self):
        street = Street("Le Clos d'Embraque")
        self.assertEqual(street.category(), "clos")

    def test_category_cours(self):
        street = Street('Cours Dillon')
        self.assertEqual(street.category(), "cours")

    def test_category_espace(self):
        street = Street('Espace du Lauragais')
        self.assertEqual(street.category(), "espace")

    def test_category_esplanade(self):
        street = Street('Esplanade Claude Cornac')
        self.assertEqual(street.category(), "esplanade")

    def test_category_impasse(self):
        street = Street('Impasse Saint-Perier')
        self.assertEqual(street.category(), "impasse")

    def test_category_lotissement(self):
        street = Street('Lotissement le Castelet')
        self.assertEqual(street.category(), "lotissement")

    def test_category_passage(self):
        street = Street('Passage Nicoulaou')
        self.assertEqual(street.category(), "passage")

    def test_category_place(self):
        street = Street('Place de la Croix-De-Pierre')
        self.assertEqual(street.category(), "place")

    def test_category_pont(self):
        street = Street('Pont Neuf')
        self.assertEqual(street.category(), "pont")

    def test_category_porte(self):
        street = Street('Porte des Sables')
        self.assertEqual(street.category(), "porte")

    def test_category_promenade(self):
        street = Street('Promenade du Bazacle')
        self.assertEqual(street.category(), "promenade")

    def test_category_rondpoint(self):
        street = Street('Rond-point de Vidailhan')
        self.assertEqual(street.category(), "rond-point")

    def test_category_rue(self):
        street = Street('Rue Boieldieu')
        self.assertEqual(street.category(), "rue")

    def test_category_route(self):
        street = Street('Route de Fronton')
        self.assertEqual(street.category(), "route")

    def test_category_square(self):
        street = Street('Square des Pins')
        self.assertEqual(street.category(), "square")

    def test_category_unknown(self):
        street = Street('Portnawak Ici')
        self.assertEqual(street.category(), "unknown")

    def test_particle_d(self):
        street = Street("Le Clos d'Embraque")
        self.assertEqual(street.name(), "embraque")

    def test_particle_de(self):
        street = Street("Route de Fronton")
        self.assertEqual(street.name(), "fronton")

    def test_particle_des(self):
        street = Street('Square des Pins')
        self.assertEqual(street.name(), "pins")

    def test_particle_du(self):
        street = Street('Rue du Puy De Grez')
        self.assertEqual(street.name(), "puy de grez")

    def test_particle_combined(self):
        street = Street("Rue de l'Armée du Salut")
        self.assertEqual(street.name(), "armée du salut")


class TestAddress(TestCase):

    def test_street(self):
        street = Street('Impasse Saint-Perier')
        address = Address(street, '2', 1.34684277748031, 43.5265939817043)
        self.assertEqual(address.street(), street)

    def test_latitude(self):
        street = Street('Impasse Saint-Perier')
        address = Address(street, '2', 1.34684277748031, 43.5265939817043)
        self.assertEqual(address.latitude(), 43.5265939817043)

    def test_longitude(self):
        street = Street('Impasse Saint-Perier')
        address = Address(street, '2', 1.34684277748031, 43.5265939817043)
        self.assertEqual(address.longitude(), 1.34684277748031)

    def test_street_addresses(self):
        street = Street('Impasse Saint-Perier')
        address1 = Address(street, '2', 1.34684277748031, 43.5265939817043)
        address2 = Address(street, '4', 1.34684277748062, 43.5265939817043)
        self.assertEqual(len(street.addresses()), 2)
        self.assertEqual(street.addresses()[0], address1)
        self.assertEqual(street.addresses()[1], address2)
