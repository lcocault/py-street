#!/usr/bin/env python3
""" Archive to store data extracted from the WikiData repository
Usage:
    See "help"
"""

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


from numpy import datetime64
from numpy import timedelta64
from elasticsearch import Elasticsearch

from wikidata import WikiDataEntity
from wikidata import WikiDataPerson


class WikiArchive:

    def __init__(self, url):
        self.es = Elasticsearch([url])

    def from_date_to_days(self, date):
        if (date is None):
            return None
        else:
            return int((date - datetime64("0000-01-01")) / timedelta64(1, 'D'))

    def indexEntity(self, entity):
        es_entity = {
            "identifier": entity.identifier(),
            "label": entity.label(),
            "type": entity.type(),
            "description": entity.description()
        }
        res = self.es.index(index='wikidata',
                            id=entity.identifier(),
                            body=es_entity)
        return res['result'] == 'created' or res['result'] == 'updated'

    def indexPerson(self, person):
        es_person = {
            "identifier": person.identifier(),
            "label": person.label(),
            "type": person.type(),
            "description": person.description(),
            "genre": person.genre(),
            "citizenship": person.citizenship(),
            "birth": self.from_date_to_days(person.birth_date()),
            "birth_location": person.birth_location(),
            "death": self.from_date_to_days(person.death_date()),
            "death_location": person.death_location()
        }
        res = self.es.index(index='wikidata',
                            id=person.identifier(),
                            body=es_person)
        return res['result'] == 'created' or res['result'] == 'updated'

    def registerError(self, id, message):
        es_error = {
            "identifier": id,
            "message": message
        }
        res = self.es.index(index='wikidata',
                            id=id,
                            body=es_error)
        return res['result'] == 'created' or res['result'] == 'updated'

    def search(self, field, value, limit):
        entities = []
        es_query = {
            "size": limit,
            "query": {
                "bool": {
                    "must": {
                        "match": {
                            field: value
                        }
                    },
                    "must_not": {
                        "match": {
                            "description": "homonymie"
                        }
                    }
                }
            }
        }
        res = self.es.search(index='wikidata', body=es_query)
        for es_entity in res["hits"]["hits"]:
            source = es_entity["_source"]
            identifier = source["identifier"]
            label = source["label"]
            type = source["type"]
            description = source["description"]
            try:
                citizenship = source["citizenship"]
                genre = source["genre"]
                birth_date = source["birth"]
                birth_location = source["birth_location"]
                death_date = source["death"]
                death_location = source["death_location"]
                entity = WikiDataPerson(identifier,
                                        label,
                                        type,
                                        description,
                                        genre,
                                        birth_date,
                                        birth_location,
                                        citizenship)
                if death_date is not None:
                    entity.die(death_date, death_location)
                entities.append(entity)
            except KeyError:
                entity = WikiDataEntity(identifier,
                                        label,
                                        type,
                                        description)
                entities.append(entity)
        return entities
