#!/usr/bin/env python3
""" Model of data from the WikiData repository
Usage:
    See "help"
"""

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


class WikiDataEntity:

    def __init__(self, identifier, label, type, description):
        self._identifier = identifier
        self._label = label
        self._type = type
        self._description = description

    def identifier(self):
        return self._identifier

    def label(self):
        return self._label

    def type(self):
        return self._type

    def description(self):
        return self._description


class WikiDataPerson(WikiDataEntity):

    def __init__(self,
                 identifier,
                 label,
                 type,
                 description,
                 genre,
                 birth_date,
                 birth_location,
                 citizenship):
        self._identifier = identifier
        self._label = label
        self._type = type
        self._description = description
        self._genre = genre
        self._birth_date = birth_date
        self._birth_location = birth_location
        self._citizenship = citizenship
        self._death_date = None
        self._death_location = None

    def birth_date(self):
        return self._birth_date

    def birth_location(self):
        return self._birth_location

    def citizenship(self):
        return self._citizenship

    def death_date(self):
        return self._death_date

    def death_location(self):
        return self._death_location

    def die(self, date, location):
        self._death_date = date
        self._death_location = location

    def genre(self):
        return self._genre

    def is_alive(self):
        return self._death_date is None


class WikiDataRiver(WikiDataEntity):

    def __init__(self,
                 identifier,
                 label,
                 type,
                 description,
                 length_in_km):
        self._identifier = identifier
        self._label = label
        self._type = type
        self._description = description
        self._length_in_km = length_in_km

    def length_in_km(self):
        return self._length_in_km
