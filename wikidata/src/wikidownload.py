#!/usr/bin/env python3
""" Extract data from the WikiData repository
Usage:
    See "help"
"""

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import argparse
import json
import sys
import urllib.error
import urllib.request

from numpy import datetime64

from wikiarchive import WikiArchive
from wikidata import WikiDataEntity
from wikidata import WikiDataPerson
from wikidata import WikiDataRiver


def convert_date(datestr):
    """Convert an extended ISO8601 date into a Python date

    Args:
        datestr: ISO8601 date

    Returns:
        A Python date
    """
    # Date format may be invalid when only the year is known
    datestr = datestr.replace("-00-", "-01-").replace("-00T", "-01T")
    return datetime64(datestr)


def parse_wikidata_entity(json_content, key):
    """Parse the WikiData entity from the given JSON content

    Args:
        json_content: JSON content representing the WikiData entity
        key: Key of the entity to parse

    Returns:
        The parsed WikiDataEntity
    """
    data = json.loads(json_content)
    entity = data["entities"][key]
    label = entity["labels"]["fr"]["value"]
    description = entity["descriptions"]["fr"]["value"]
    atts = entity["claims"]
    type = atts["P31"][0]["mainsnak"]["datavalue"]["value"]["id"]
    try:
        born = atts["P569"][0]["mainsnak"]["datavalue"]["value"]["time"]
        birth_place = atts["P19"][0]["mainsnak"]["datavalue"]["value"]["id"]
        genre = atts["P21"][0]["mainsnak"]["datavalue"]["value"]["id"]
        citizenship = atts["P27"][0]["mainsnak"]["datavalue"]["value"]["id"]
        result = WikiDataPerson(key,
                                label,
                                type,
                                description,
                                genre,
                                convert_date(born),
                                birth_place,
                                citizenship)
        try:
            died = atts["P570"][0]["mainsnak"]["datavalue"]["value"]["time"]
            died_place = atts["P20"][0]["mainsnak"]["datavalue"]["value"]["id"]
            result.die(convert_date(died), died_place)
        except KeyError:
            pass
    except KeyError:
        try:
            len = atts["P2043"][0]["mainsnak"]["datavalue"]["value"]["amount"]
            lengthInKm = float(len)
            result = WikiDataRiver(key,
                                   label,
                                   type,
                                   description,
                                   lengthInKm)
        except KeyError:
            result = WikiDataEntity(key, label, type, description)
    return result


def download_wikidata_entity(key):
    """Extract the WikiData entity having the given key

    Args:
        key: Key of the entity to extract

    Returns:
        The extracted WikiDataEntity
    """
    baseurl = "https://www.wikidata.org/wiki/Special:EntityData/{}.json"
    wikiurl = baseurl.format(key)
    with urllib.request.urlopen(wikiurl) as url:
        return parse_wikidata_entity(url.read().decode(), key)


def download_wikidata_entities(first, last, store):
    """Download data from the WikiData reference entities

    Args:
        first: Number of the first entity to extract
        last: Number of the last entity to extract
        store: URL of the archive to store the extracted entities

    Returns:
        Void
    """
    archive = WikiArchive(store)
    for i in range(first, last):
        if i % 100 >= 0:
            print("Retrieving entity {}".format(i))
            sys.stdout.flush()
        try:
            entity = download_wikidata_entity("Q{}".format(i))
            try:
                archive.indexPerson(entity)
            except AttributeError:
                archive.indexEntity(entity)
        except urllib.error.HTTPError:
            pass
        except KeyError as error:
            archive.registerError(i, str(error))
            pass
        except ValueError as error:
            archive.registerError(i, str(error))
            pass


def update_wikidata_entities(query, store):
    """Update WikiData reference entities

    Args:
        query: Query to select the records to update
        store: URL of the archive to query and store the updated entities

    Returns:
        Void
    """
    archive = WikiArchive(store)
    args = query.split(",")
    entities = archive.search(args[0], args[1], args[2])
    for entity in entities:
        print("Retrieving entity {}".format(entity.identifier()))
        entity = download_wikidata_entity(entity.identifier())
        try:
            archive.indexPerson(entity)
        except AttributeError:
            archive.indexEntity(entity)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--first",
        "-f",
        type=int,
        default=41,
        help="Number of the first record to read")
    parser.add_argument(
        "--last",
        "-l",
        type=int,
        default=43,
        help="Number of the last record to read")
    parser.add_argument(
        "--store",
        "-s",
        type=str,
        default='localhost:9200',
        help="URL of the store to use")
    parser.add_argument(
        "--query",
        "-q",
        type=str,
        default='',
        help="Query to select records to update")
    args = parser.parse_args()
    if args.query == '':
        download_wikidata_entities(args.first, args.last, args.store)
    else:
        update_wikidata_entities(args.query, args.store)
