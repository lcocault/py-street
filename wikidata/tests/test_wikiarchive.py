#!/usr/bin/env python3
""" Test the WikiArchive class """

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from numpy import datetime64
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError
from unittest import TestCase

from wikiarchive import WikiArchive
from wikidata import WikiDataEntity
from wikidata import WikiDataPerson


class TestWikiArchive(TestCase):

    def setUp(self):
        self.url = "localhost:9200"
        self.entitykey = "Qentity"
        self.secondkey = "Qother"
        self.personkey = "Qperson"
        self.es = Elasticsearch([self.url])
        for key in (self.entitykey, self.secondkey, self.personkey):
            try:
                self.es.delete(index='wikidata', id=key)
            except NotFoundError:
                pass

    def test_index_entity_once(self):
        entity = WikiDataEntity(self.entitykey, 'Toulouse', 'Q3', 'city')
        archive = WikiArchive(self.url)
        indexed = archive.indexEntity(entity)
        self.assertTrue(indexed)

    def test_index_entity_twice(self):
        entity = WikiDataEntity(self.entitykey, 'Paris', 'Q3', 'city')
        archive = WikiArchive(self.url)
        indexed = archive.indexEntity(entity)
        self.assertTrue(indexed)
        indexed = archive.indexEntity(entity)
        self.assertTrue(indexed)

    def test_index_person_once(self):
        person = WikiDataPerson(self.personkey,
                                'Douglas Adams',
                                'Q5',
                                'écrivain anglais',
                                'homme',
                                datetime64('+1952-03-11T00:00:00Z'),
                                'Q350',
                                'Q145')
        person.die(datetime64('+2001-05-11T00:00:00Z'), 'Q159288')
        archive = WikiArchive(self.url)
        indexed = archive.indexPerson(person)
        self.assertTrue(indexed)

    def test_index_person_twice(self):
        person = WikiDataPerson(self.personkey,
                                'Douglas Adams',
                                'Q5',
                                'écrivain anglais',
                                'homme',
                                datetime64('+1952-03-11T00:00:00Z'),
                                'Q350',
                                'Q145')
        person.die(datetime64('+2001-05-11T00:00:00Z'), 'Q159288')
        archive = WikiArchive(self.url)
        indexed = archive.indexPerson(person)
        self.assertTrue(indexed)
        indexed = archive.indexPerson(person)
        self.assertTrue(indexed)

    def test_index_live_person(self):
        person = WikiDataPerson(self.personkey,
                                'Douglas Adams',
                                'Q5',
                                'écrivain anglais',
                                'homme',
                                datetime64('+1952-03-11T00:00:00Z'),
                                'Q350',
                                'Q145')
        archive = WikiArchive(self.url)
        indexed = archive.indexPerson(person)
        self.assertTrue(indexed)

    def test_register_error(self):
        archive = WikiArchive(self.url)
        registered = archive.registerError("Q666", "Failed to parse date")
        self.assertTrue(registered)

    def test_search(self):
        archive = WikiArchive(self.url)
        entity1 = WikiDataEntity(self.entitykey, 'Paris', 'Q3', 'city')
        entity2 = WikiDataEntity(self.secondkey, 'Toulouse', 'Q3', 'city')
        archive.indexEntity(entity1)
        archive.indexEntity(entity2)
        entities = archive.search("type", "Q2", 1000)
        self.assertEqual(len(entities), 0)
        entities = archive.search("type", "Q3", 1000)
        self.assertEqual(len(entities), 2)
