#!/usr/bin/env python3
""" Test the WikiData classes """

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from unittest import TestCase
from wikidata import WikiDataEntity
from wikidata import WikiDataPerson


class TestWikiDataEntity(TestCase):

    def test_identifier(self):
        entity = WikiDataEntity("Q123", 'Toulouse', 'Q3', 'city of France')
        self.assertEqual(entity.identifier(), "Q123")

    def test_label(self):
        entity = WikiDataEntity("Q123", 'Toulouse', 'Q3', 'city of France')
        self.assertEqual(entity.label(), 'Toulouse')

    def test_type(self):
        entity = WikiDataEntity("Q123", 'Toulouse', 'Q3', 'city of France')
        self.assertEqual(entity.type(), 'Q3')

    def test_description(self):
        entity = WikiDataEntity("Q123", 'Toulouse', 'Q3', 'city of France')
        self.assertEqual(entity.description(), 'city of France')


class TestWikiDataPerson(TestCase):

    def setUp(self):
        self.entity = WikiDataPerson('Q42',
                                     'Douglas Adams',
                                     'Q5',
                                     'writer',
                                     'man',
                                     '1952-03-11',
                                     'Q350',
                                     'Q145')

    def test_label(self):
        self.assertEqual(self.entity.label(), 'Douglas Adams')

    def test_type(self):
        self.assertEqual(self.entity.type(), 'Q5')

    def test_description(self):
        self.assertEqual(self.entity.description(), 'writer')

    def test_birth_date(self):
        self.assertEqual(self.entity.birth_date(), '1952-03-11')

    def test_birth_place(self):
        self.assertEqual(self.entity.birth_location(), 'Q350')

    def test_citizenship(self):
        self.assertEqual(self.entity.citizenship(), 'Q145')

    def test_genre(self):
        self.assertEqual(self.entity.genre(), 'man')

    def test_live(self):
        self.assertEqual(self.entity.death_date(), None)
        self.assertEqual(self.entity.death_location(), None)
        self.assertTrue(self.entity.is_alive())

    def test_die(self):
        self.entity.die('2001-05-11', 'Q159288')
        self.assertEqual(self.entity.death_date(), '2001-05-11')
        self.assertEqual(self.entity.death_location(), 'Q159288')
        self.assertFalse(self.entity.is_alive())


class TestWikiDataRiver(TestCase):

    def setUp(self):
        self.entity = WikiDataPerson('Q5077',
                                     'Garonne',
                                     'Q4022',
                                     'River',
                                     647)

    def test_label(self):
        self.assertEqual(self.entity.label(), 'Garonne')

    def test_type(self):
        self.assertEqual(self.entity.type(), 'Q4022')

    def test_description(self):
        self.assertEqual(self.entity.description(), 'River')

    def test_length_in_km(self):
        self.assertEqual(self.entity.length_in_km(), 647)
