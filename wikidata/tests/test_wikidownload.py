#!/usr/bin/env python3
""" Test the WikiDownload classes """

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from numpy import datetime64
from unittest import TestCase
from wikidownload import download_wikidata_entity
from wikidownload import parse_wikidata_entity


class TestWikiDownload(TestCase):

    def setUp(self):
        # Test URL is a local path
        self._test_url = "file://./{}.json"

    def test_parse_dead_person(self):
        key = "Q42"
        with open('tests/{}.json'.format(key)) as json_file:
            entity = parse_wikidata_entity(json_file.read(), key)
            self.assertEqual(entity.label(), "Douglas Adams")
            self.assertEqual(entity.type(), "Q5")
            self.assertEqual(entity.description(),
                             "écrivain anglais de science-fiction")
            self.assertEqual(entity.citizenship(), "Q145")
            self.assertEqual(entity.birth_date(), datetime64("1952-03-11"))
            self.assertEqual(entity.birth_location(), "Q350")
            self.assertEqual(entity.death_date(), datetime64("2001-05-11"))
            self.assertEqual(entity.death_location(), "Q159288")
            self.assertFalse(entity.is_alive())

    def test_parse_bc_person(self):
        key = "Q1048"
        with open('tests/{}.json'.format(key)) as json_file:
            entity = parse_wikidata_entity(json_file.read(), key)
            self.assertEqual(entity.label(), "Jules César")
            self.assertEqual(entity.type(), "Q5")
            self.assertEqual(entity.citizenship(), "Q1747689")
            self.assertEqual(entity.birth_date(), datetime64("-100-07-01"))
            self.assertEqual(entity.birth_location(), "Q220")
            self.assertEqual(entity.death_date(), datetime64("-44-03-15"))
            self.assertEqual(entity.death_location(), "Q220")
            self.assertFalse(entity.is_alive())

    def test_parse_alive_person(self):
        key = "Q3592228"
        with open('tests/{}.json'.format(key)) as json_file:
            entity = parse_wikidata_entity(json_file.read(), key)
            self.assertEqual(entity.label(), "Étienne Klein")
            self.assertEqual(entity.type(), "Q5")
            self.assertEqual(entity.description(),
                             "philosophe des sciences français")
            self.assertEqual(entity.citizenship(), "Q142")
            self.assertEqual(entity.birth_date(), datetime64("1958-04-01"))
            self.assertEqual(entity.birth_location(), "Q90")
            self.assertEqual(entity.death_date(), None)
            self.assertTrue(entity.is_alive())

    def test_parse_river(self):
        key = "Q5077"
        with open('tests/{}.json'.format(key)) as json_file:
            entity = parse_wikidata_entity(json_file.read(), key)
            self.assertEqual(entity.type(), "Q4022")
            self.assertEqual(entity.label(), "Garonne")
            self.assertEqual(entity.description(), "fleuve d'Europe")
            self.assertEqual(entity.length_in_km(), 647)

    def test_parse_entity(self):
        key = "Q41"
        with open('tests/{}.json'.format(key)) as json_file:
            entity = parse_wikidata_entity(json_file.read(), key)
            self.assertEqual(entity.type(), "Q3624078")
            self.assertEqual(entity.label(), "Grèce")
            self.assertEqual(entity.description(), "pays d'Europe")

    def test_download_person(self):
        entity = download_wikidata_entity("Q42")
        self.assertEqual(entity.label(), "Douglas Adams")
